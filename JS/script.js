"use strict";

/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
2. Яка різниця між event.code() та event.key()?
3. Які три події клавіатури існує та яка між ними відмінність?

Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, 
повинна фарбуватися в синій колір. При цьому якщо якась 
інша літера вже раніше була пофарбована в синій колір - 
вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у 
синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, 
а кнопка Enter знову стає чорною.
*/

console.log("THEORY");

console.log("1. window.addEventListener. буде трекати всі дії у вікні, на кожну дію в консолі можна подивитися деталі");
console.log("2. event.code() показує, яка клавіша була натиснута, event.key() відповідає за значення клавіші, значення може змінюватися, наприклад, при перемиканні на іншу мову");
console.log("3. keydown i keyup - натиснути і відпустити клавішу, keypress - стара версія");


console.log("PRACTICE");


// const test = document.querySelector('.btn')
// let test = document.getElementsByClassName("btn");
window.addEventListener("keydown", (event) => {
    console.log(event);
    let test = document.getElementsByClassName("btn");
    const test1 = document.querySelector(".active");

    if (test1) {
        test1.classList.remove("active");
    }

    switch (event.code) {
        case 'Enter':  // if (x === 'value1')
            test[0].classList.add("active");
            break;

        case 'KeyS':  // if (x === 'value2')
            test[1].classList.add("active");
            break;

        case 'KeyE':  // if (x === 'value2')
            test[2].classList.add("active");
            break;

        case 'KeyO':  // if (x === 'value2')
            test[3].classList.add("active");
            break;

        case 'KeyN':  // if (x === 'value2')
            test[4].classList.add("active");
            break;

        case 'KeyL':  // if (x === 'value2')
            test[5].classList.add("active");
            break;
        case 'KeyZ':  // if (x === 'value2')
            test[6].classList.add("active");
            break;

        case 'Tab':  // if (x === 'value2')
            test[7].classList.add("active");
            break;

        default:
            alert("No colours, no sense");
            break;
    }
});





/* window.addEventListener('keydown', (event) => {
    console.log(first);
    const activeElem = document.querySelector('.active');

    if (activeElem) {
        activeElem.classList.remove('active');
    }

    el = document.querySelector(`#${event.code.toLowerCase()}-counter`);

    if (el) {
        el.innerText = +el.innerText + 1;
        el.classList.add('active');
    }
}) */



